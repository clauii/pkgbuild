# Maintainer: Claudia Pellegrino <aur ät cpellegrino.de>
# Contributor:

pkgname=confbuild-git
pkgver=r8.104f054
pkgrel=3
pkgdesc='Configuration manager with ALPM hooks'
arch=('any')
url='https://bitbucket.org/clauii/confbuild'
license=('custom:MIT')
makedepends=('git')
optdepends=('confbuild-dhcp: Track upstream DHCP package updates')
_rundepends=('xplatform')
provides=('confbuild')
conflicts=('confbuild')

source=(
  "${pkgname}::git+ssh://git@bitbucket.org/clauii/${pkgname%-git}.git"
  'confbuild-dhcp-pre.hook'
  'confbuild-dhcp-post.hook'
)

sha512sums=(
  'SKIP'
  'SKIP'
  'SKIP'
)

pkgver() {
  printf "r%s.%s" \
    "$(git -C "${pkgname}" rev-list --count HEAD)" \
    "$(git -C "${pkgname}" rev-parse --short HEAD)"
}

build() {
  # Prepare binstub
  mkdir -p "${srcdir}"
  printf '#!/bin/bash\n%s\n' > "${srcdir}/binstub" \
    'exec "/usr/lib/'"${pkgname}"'/bin/$(basename "${0}")" "$@"'
}

package() {
  # TODO: Uncomment the following line as soon as xplatform is a proper package
  # depends=("${_rundepends[@]}")

  msg2 'Installing the license'
  install -D -m 644 -t "${pkgdir}/usr/share/licenses/${pkgname}" \
    "${srcdir}/${pkgname}/LICENSE.md"

  msg2 'Installing package files'
  mkdir -p "${pkgdir}/usr/lib/${pkgname}"
  cp -r --preserve=mode -t "${pkgdir}/usr/lib/${pkgname}" \
    "${srcdir}/${pkgname}/"{bin,libexec}

  # TODO: Remove this patch as soon as xplatform is a proper package
  msg2 'Applying temporary patch'
  sed -i 's/xchmod/chmod/' "${pkgdir}/usr/lib/${pkgname}/libexec/confbuild.bash"

  msg2 'Installing ALPM hook'
  mkdir -p "${pkgdir}/usr/share/libalpm/hooks"
  install -D -m 644 \
    "${srcdir}/confbuild-"*".hook" \
    "${pkgdir}/usr/share/libalpm/hooks/"

  msg2 'Installing binstub'
  mkdir -p "${pkgdir}/usr/bin"
  install -D -m 755 -T "${srcdir}/binstub" \
    "${pkgdir}/usr/bin/confbuild"

  msg2 'Installing documentation'
  install -D -m 644 -t "${pkgdir}/usr/share/doc/${pkgname}" \
    "${srcdir}/${pkgname}/README.md"
}
